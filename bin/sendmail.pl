#!/home/sh8/s3105178/.opt/perlbrew/perls/perl-5.18.2/bin/perl

use strict;
use warnings;

use open qw(:std :utf8);
use constant LOG => '/home/sh8/s3105178/rt-outbound.log';

use WebService::Mandrill ();

open my $log, '>>', LOG;

my $mail = join('', <STDIN>);
my $time = scalar(localtime);
print $log '# ', scalar(localtime), ' ', ('#' x (68 - length $time)), "\n\n";
# print $log "\@ARGV = [@ARGV]\n\n";
print $log "$mail\n";

my $mandrill = WebService::Mandrill->new(
  api_key => '505eGBpH9HZZgXy7HX8gvw',
# debug   => 1,
);


my $parse = $mandrill->parse( raw_message => $mail );
# use YAML; print $log Dump($parse), "\n";

my @addrs;
push @addrs, map { $_->{'email'} } @{ $parse->{'content'}->{'to'} }
  if $parse->{'content'}->{'to'};
push @addrs, map { (ref $_ eq ref []) ? $_->[0] : $_->{'email'} } @{ $parse->{'content'}->{'cc'} }
  if $parse->{'content'}->{'cc'};

if (exists $parse->{'content'}->{'headers'}->{'Bcc'}) {
  my $mockmail = sprintf(
    "To: %s\n",
    $parse->{'content'}->{'headers'}->{'Bcc'},
  );
  $parse = $mandrill->parse( raw_message => $mockmail );
# use YAML; print $log Dump($parse), "\n";
  push @addrs, map { $_->{'email'} } @{ $parse->{'content'}->{'to'} };
}
use List::MoreUtils 'uniq'; @addrs = uniq @addrs;
# use YAML; print $log Dump([@addrs]);

#   headers:
#     Bcc: 's3105178@student.rmit.edu.au, s3283616@student.rmit.edu.au, s3297861@student.rmit.edu.au, s3338291@student.rmit.edu.au, s3345487@student.rmit.edu.au'
# 
#   to: ~
#   cc:
#     -
#       - alex@peters.net
#       - ~
# 
# 
# 
# 
# my @to = $parse->{'

my $response = $mandrill->send_raw( raw_message => $mail, to => \@addrs );
delete $response->{$_} for qw{ header raw };
use YAML; print $log Dump($response), "\n";

close $log;
