# rt-local

Congestion Crowdsensing Project ("CCP")  
Local [Request Tracker] ("RT") customisations for the [CCP RT] instance

## Author

Alex Peters  
<s3105178@student.rmit.edu.au>

## Repository contents

### bin/sendmail.pl

In order to send outgoing mail, RT requires access to a
Sendmail-compatible executable or an SMTP server.  The
`coreteaching01.csit` system has no working mail transfer agent ("MTA")
and forbids connections to foreign MTAs.

This script provides a Sendmail-style interface to the [Mandrill][]
[HTTP API][Mandrill API], permitting RT to send outgoing mail.

### cgi-bin/rt.cgi

RT is typically deployed as a [FastCGI] server, but the
`coreteaching01` system only offers the CGI protocol.

This script functions as a CGI gateway to RT, permitting web access to
RT.

### cgi-bin/rt-bitbucket.cgi

[Bitbucket] offers [hooks][Bitbucket hooks] to trigger custom actions
when commits are pushed.  [One such hook][Bitbucket POST hook]
publishes information via HTTP POST.

This script functions as a CGI gateway to a processor of that
information, permitting commit information that references one or more
specific RT tickets (e.g. `RT #123`) to be added automatically to those
tickets.

### cgi-bin/rt-inbound.cgi

RT can accept incoming mail and create or update tickets accordingly,
but the `coreteaching01` system has no publicly accessible email
addresses.

This script functions as a CGI gateway to accept incoming mail from
[Mandrill] via HTTP POST, permitting RT to process incoming mail.

### etc/RT_SiteConfig.pm

This file contains all of the configuration values specific to the [CCP
RT] instance.

### man/*

This directory contains [`man` pages][man page] for the included
plugins (discussed below).

### plugins/RT-Extension-MergeUsers/*

RT users can have only one email address associated with them.  If an
RT user communicates with RT using an email address not known to RT, RT
treats the communication as coming from a new user and therefore
doesn't grant the correct rights to that transaction.

The [RT-Extension-MergeUsers] extension "links" these separate users,
permitting users to communicate with RT using multiple email addresses.

[Bitbucket]: https://bitbucket.org/
[Bitbucket hooks]: https://confluence.atlassian.com/x/BA4zDQ
[Bitbucket POST hook]: https://confluence.atlassian.com/display/BITBUCKET/POST+hook+management
[CCP RT]: http://coreteaching01.csit.rmit.edu.au/~s3105178/sep/rt.cgi
[FastCGI]: http://en.wikipedia.org/wiki/FastCGI
[Mandrill]: http://mandrill.com/
[Mandrill API]: https://mandrillapp.com/api/docs/index.html
[man page]: http://en.wikipedia.org/wiki/Man_page
[Request Tracker]: http://bestpractical.com/rt/
[RT-Extension-MergeUsers]: https://metacpan.org/pod/RT::Extension::MergeUsers
