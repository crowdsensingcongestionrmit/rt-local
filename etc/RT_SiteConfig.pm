# BASICS ##############################################################

# Displayed in the top-right corner of the RT web interface, and is
# also the default subject tag in outgoing emails.
Set($rtname, 'Congestion Crowdsensing');

# Affects default display of times in the web interface.
Set($Timezone, 'Australia/Melbourne');

# "Used by the linking interface to guarantee that ticket URIs are
# unique and easy to construct."  Cannot be changed without breaking
# all existing ticket links.  Expected to be unique for every RT
# system.  Seems only to be exposed in email headers.
Set($Organization, 'sep-rt.alexpeters.net');

# Allow mail from multiple email addresses to be treated as belonging
# to one RT user.
Plugin('RT::Extension::MergeUsers');

# Suppress warnings about some missing system components.
Set($DisableGD, 1);
Set($DisableGraphViz, 1);

# WEB INTERFACE #######################################################

# Where links should point.
Set($WebDomain, 'coreteaching01.csit.rmit.edu.au');
Set($WebPath, '/~s3105178/sep/rt.cgi');

# If no other queue is specified or explicitly selected, make new
# tickets go into this queue.
Set($DefaultQueue, 'Tasks');

# Disable the Reminders portlets and links.
Set($EnableReminders, 0);

# A slightly more useful default search result format for our setup.
Set($DefaultSearchResultFormat, q{
    '<b><a href="__WebPath__/Ticket/Display.html?id=__id__">__id__</b></a>',
    '<b><a href="__WebPath__/Ticket/Display.html?id=__id__">__Subject__</b></a>',
    '__ExtendedStatus__',
    '__OwnerName__',
    '__Requestors__',
    '__DueRelative__',
    '__LastUpdatedRelative__',
    '__LastUpdatedBy__/TITLE:...by',
    '__NEWLINE__',
    '__NBSP__',
    '<small>__QueueName__</small>',
    '<small>__UpdateStatus__</small>/TITLE:Unread',
    '<small>__CreatedBy__</small>/TITLE:Creator',
    '<small>__Cc__</small>',
    '<small>__Due__</small>/TITLE:',
    '<small>__LastUpdated__</small>/TITLE:',
    '__NBSP__',
});

# Show mobile numbers on User Summary pages and in user search results.
Set($UserSummaryExtraInfo, 'Name, RealName, MobilePhone, EmailAddress');
Set($UserSearchResultFormat, q{
    '<a href="__WebPath__/User/Summary.html?id=__id__">__id__</a>/TITLE:#',
    '<a href="__WebPath__/User/Summary.html?id=__id__">__Name__</a>/TITLE:Name',
    __RealName__,
    __MobilePhone__,
    __EmailAddress__
});

# Only show short usernames in the interface.
Set($UsernameFormat, 'concise');

# Increased from the default because some emails weren't being shown in
# the web interface.
Set($MaxInlineBody, 20_000);

# MAIL ################################################################

# CorrespondAddress MUST be set, or email sending will fail.
Set($CorrespondAddress, 'sep-rt@38gr.net');
Set($CommentAddress, 'sep-rt-quiet@38gr.net');

Set($OwnerEmail, 's3105178@student.rmit.edu.au');

Set($RTAddressRegexp, qr/^sep-rt\@38gr\.net$/);

Set(
    $SendmailPath,
    '/home/sh8/s3105178/.opt/rt-4.2.3/local/bin/sendmail.pl',
);
Set($SendmailArguments, '');

# LOGGING #############################################################

# Log anything that might be interesting to a local file.
Set($LogToFile, 'info');
Set($LogDir, '/home/sh8/s3105178/.opt/rt-4.2.3/var/log');
Set($LogToFileNamed, 'rt.log');

# Avoid logging anything at all to either the syslog or the web server
# log.
Set($LogToSyslog, 'emergency');
Set($LogToSTDERR, 'emergency');

#######################################################################

1;
