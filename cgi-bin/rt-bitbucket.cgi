#!/home/sh8/s3105178/.opt/perlbrew/perls/perl-5.18.2/bin/perl

open STDERR, '>>', '/home/sh8/s3105178/rt-bitbucket.log';

my $time;
BEGIN { $time = scalar(localtime); }

use CGI ();
use JSON 'decode_json';
use List::MoreUtils 'uniq';
use YAML 'Dump';

use lib '/home/sh8/s3105178/.opt/rt-4.2.3/lib';
use RT ();
use RT::Date ();
use RT::Interface::CLI ();
use RT::Ticket ();
use RT::User ();
BEGIN {
  RT::Interface::CLI->CleanEnv;
  RT->LoadConfig;
  RT->Init;
}

my $q = CGI->new;
print $q->header('text/plain');

my $data = $q->param('payload');
eval { $data = decode_json($data) };

if ($@) {
  print $@;
}
else {
  print "OK\n";

  for my $commit (@{ $data->{'commits'} }) {
    my $excerpt = '(no commit message)';
    if (
      defined $commit->{'message'}
      and $commit->{'message'} =~ /\S/
    ) {
      $excerpt = $commit->{'message'};
      $excerpt =~ s/^\s+//;
      $excerpt =~ s/\n.*//s;
      $excerpt =~ s/\s+/ /g;
      $excerpt =~ s/\s+$//;
      $excerpt = substr($excerpt, 0, 50) . '...'
        if length $excerpt > 50;
    }
    my $url = sprintf(
      '%s%scommits/%s',
      $data->{'canon_url'},
      $data->{'repository'}->{'absolute_url'},
      $commit->{'node'},
    );
    my @tickets = uniq ($commit->{'message'} =~ /\bRT\s*#?\s*(\d+)/gi);
    push @{ $data->{'__emails'} }, {
      author  => $commit->{'raw_author'},
      excerpt => $excerpt,
      files   => $commit->{'files'},
      message => $commit->{'message'},
      tickets => \@tickets,
      time    => $commit->{'utctimestamp'},
      url     => $url,
    };
  }
}

open my $f, '>>', '/home/sh8/s3105178/rt-bitbucket.log';
print { $f } (
  '# ', $time, ' ', ('#' x (68 - length $time)), "\n\n",
  Dump($data), "\n",
);
close $f;

for my $email (@{ $data->{'__emails'} }) {
  my ($address) = ($email->{'author'} =~ /\<(.*)\>/);
  my $me = 1;
  my $user = RT::User->new;
  $user->LoadByEmail($address);
  $me = 0 if not $user->Id;
  $user = RT->SystemUser if not $user->Id;
  for my $ticket_num (@{ $email->{'tickets'} }) {
    my $ticket = RT::Ticket->new($user);
    $ticket->Load($ticket_num);
    my @affected_files;
    for my $file (@{ $email->{'files'} }) {
      my $filename = $file->{'file'};
      my $change = uc substr($file->{'type'}, 0, 1);
      push @affected_files, "$change $filename";
    }
    my $files = join("\n", @affected_files);
    my $date = RT::Date->new($user);
    $email->{'time'} =~ s/\+00:00$//;
    $date->Set(
      Format  => 'iso',
      Value   => $email->{'time'},
    );
    my $msg = sprintf(
      "Commit at %s %s%s:\n%s\n%s\n\nAffected files:\n%s",
      $date->LocalizedDateTime( Timezone => 'user' ),
      $date->Timezone('user'),
      ($me ? '' : sprintf(' by %s', $email->{'author'})),
      $email->{'message'},
      $email->{'url'},
      $files,
    );

    open my $f, '>>', '/home/sh8/s3105178/rt-bitbucket.log';
    print { $f } $msg, "\n\n";
    close $f;

    $ticket->AddLink(
      Type    => 'RefersTo',
      Target  => $email->{'url'},
    );
    $ticket->Comment( Content => $msg );
  }
}
