#!/home/sh8/s3105178/.opt/perlbrew/perls/perl-5.18.2/bin/perl

use CGI ();
use IPC::Run 'run';
use JSON 'decode_json';
use YAML 'Dump';

my $q = CGI->new;
print $q->header('text/plain');

my @msgs
  = map { $_->{'msg'} } @{ decode_json($q->param('mandrill_events')) };
for my $msg (@msgs) {
  my $time = scalar(localtime);
  my $raw_msg = delete $msg->{'raw_msg'};
  my $action
    = $msg->{'email'} eq 'sep-rt-quiet@38gr.net'
    ? 'comment'
    : 'correspond';
  my @cmd = (
    '/home/sh8/s3105178/.opt/rt-4.2.3/bin/rt-mailgate',
    '--action',
    $action,
    '--queue',
    'Group',
    '--url',
    'http://coreteaching01.csit.rmit.edu.au/~s3105178/sep/rt.cgi',
  );
  run \@cmd, \$raw_msg;

  delete $msg->{'html'};
  delete $msg->{'text'};
  open my $f, '>>', '/home/sh8/s3105178/rt-inbound.log';
  print { $f } (
    '# ', $time, ' ', ('#' x (68 - length $time)), "\n\n",
    $raw_msg, "\n",
    Dump({ email => $msg->{'email'}, cmd => "@cmd", ret => $? }), "\n",
  );
  close $f;

  print "OK\n";
}
